import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AuthModule} from './auth/auth.module';
import {LoginComponent} from './login/login.component';
import {UserManagementService} from './_services/user-management/user-management.service';
import {HomePageComponent} from './home-page/home-page.component';
import {RegisterComponent} from './register/register.component';
import {MomentPipe} from './_pipes/moment.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MilestoneFormComponent} from './home-page/milestone-form/milestone-form.component';
import {RangePipe} from './_pipes/range.pipe';
import {MilestoneService} from './_services/milestone/milestone.service';
import {MilestonePageComponent} from './milestone-page/milestone-page.component';
import {IssueService} from './_services/issue/issues.service';
import {IssueCardComponent} from './milestone-page/issue-card/issue-card.component';
import {IssueFormComponent} from './milestone-page/issue-form/issue-form.component';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomePageComponent,
    MilestoneFormComponent,
    MomentPipe,
    RangePipe,
    MilestonePageComponent,
    IssueCardComponent,
    IssueFormComponent
  ],
  imports: [
    AuthModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
  ],
  entryComponents: [MilestoneFormComponent, IssueFormComponent],
  providers: [UserManagementService, MilestoneService, IssueService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
