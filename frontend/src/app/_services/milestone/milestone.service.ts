import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Milestone, ItemsList, Pagination} from '../../app.types';
import {CreateMilestoneRequest} from './types';

@Injectable()
export class MilestoneService {

  constructor(private http: HttpClient) {
  }

  getMilestone(id: string): Observable<Milestone> {
    return this.http.get<Milestone>(`milestone/${id}`, {});
  }

  getMilestones(pagination: Pagination): Observable<ItemsList<Milestone>> {
    const params = new HttpParams({
      fromObject: {
        page: pagination.page.toLocaleString(),
        pageSize: pagination.pageSize.toLocaleString()
      }
    });
    return this.http.get<ItemsList<Milestone>>('milestone', {params});
  }

  createMilestone(request: CreateMilestoneRequest): Observable<Milestone> {
    return this.http.post<Milestone>('milestone', request);
  }

  removeMilestone(id: string): Observable<void> {
    return this.http.delete<void>(`milestone/${id}`, {});
  }

}
