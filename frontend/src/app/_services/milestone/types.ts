export interface CreateMilestoneRequest {
  name: string;
  description: string;
  startsAt: string;
  endsAt: string;
}
