export interface CreateIssueRequest {
  title: string;
  description: string;
}
