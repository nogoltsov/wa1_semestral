import {Injectable} from '@angular/core';
import {Issue, ItemsList, Pagination} from '../../app.types';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CreateIssueRequest} from './types';

@Injectable()
export class IssueService {

  constructor(private http: HttpClient) {
  }

  getIssue(milestoneId: string, issueId: string): Observable<Issue> {
    return this.http.get<Issue>(`milestone/${milestoneId}/issue/${issueId}`, {});
  }

  getIssues(milestoneId: string, pagination: Pagination): Observable<ItemsList<Issue>> {
    const params = new HttpParams({
      fromObject: {
        page: pagination.page.toLocaleString(),
        pageSize: pagination.pageSize.toLocaleString()
      }
    });
    return this.http.get<ItemsList<Issue>>(`milestone/${milestoneId}/issue`, {params});
  }

  createIssue(milestoneId: string, request: CreateIssueRequest): Observable<Issue> {
    return this.http.post<Issue>(`milestone/${milestoneId}/issue`, request);
  }

  removeIssue(milestoneId: string, issueId: string): Observable<void> {
    return this.http.delete<void>(`milestone/${milestoneId}/issue/${issueId}`, {});
  }

}
