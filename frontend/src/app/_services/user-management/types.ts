export interface RegisterRequest {
  name: string;
  username: string;
  password: string;
}

export interface RegisterResponse {
  user: {
    token: string;
  };
}

export interface LoginRequest {
  username: string;
  password: string;
}

export interface LoginResponse {
  user: {
    token: string;
  };
}
