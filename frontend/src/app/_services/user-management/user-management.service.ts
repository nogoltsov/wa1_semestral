import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {LoginRequest, LoginResponse, RegisterRequest, RegisterResponse} from './types';

@Injectable()
export class UserManagementService {

  constructor(private http: HttpClient) {
  }

  checkIfUserExists(username: string): Observable<boolean> {
    return this.http.get<boolean>('auth/check-user-existence', {params: new HttpParams().set('username', username)});
  }

  registerUser(user: RegisterRequest): Observable<RegisterResponse> {
    return this.http.post<RegisterResponse>('auth/register', user);
  }

  loginUser(user: LoginRequest): Observable<LoginResponse> {
    return this.http.post<LoginResponse>('auth/login', user);
  }

  logoutUser(): Observable<void> {
    return this.http.post<void>('auth/logout', {});
  }

}
