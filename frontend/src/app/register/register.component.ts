import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth/auth.service';
import 'rxjs/add/operator/catch';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {UserManagementService} from '../_services/user-management/user-management.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-signup',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  loading = false;
  form: FormGroup;

  get name(): AbstractControl {
    return this.form.controls.name;
  }

  get username(): AbstractControl {
    return this.form.controls.username;
  }

  get password(): AbstractControl {
    return this.form.controls.password;
  }

  get repeatPassword(): AbstractControl {
    return this.form.controls.repeatPassword;
  }

  constructor(private authService: AuthService,
              private userManagementService: UserManagementService,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required, Validators.minLength(5)], this.usernameOccupied.bind(this)),
      password: new FormControl('', [Validators.required, Validators.minLength(5)]),
      repeatPassword: new FormControl('', [this.matchOtherValidator('password')])
    });

    // reset login status
    this.authService.logout();
  }

  confirm() {
    this.loading = true;
    this.authService.register(this.form.controls.name.value, this.form.controls.username.value, this.form.controls.password.value)
      .subscribe(res => {
          this.loading = false;
          // navigate to the Home page
          this.router.navigate(['/home']);
        },
        error => {
          this.loading = false;
          this.toastr.error('Please try again', 'Registration failed');
        }
      );
  }


  // https://stackoverflow.com/a/44088196
  matchOtherValidator(otherControlName: string) {
    let thisControl: FormControl;
    let otherControl: FormControl;

    return function matchOtherValidate(control: FormControl) {
      if (!control.parent) {
        return null;
      }
      // Initializing the validator.
      if (!thisControl) {
        thisControl = control;
        otherControl = control.parent.get(otherControlName) as FormControl;
        if (!otherControl) {
          throw new Error('matchOtherValidator(): other control is not found in parent group');
        }
        otherControl.valueChanges.subscribe(() => {
          thisControl.updateValueAndValidity();
        });
      }
      if (!otherControl) {
        return null;
      }
      if (otherControl.value !== thisControl.value) {
        return {
          matchOther: true
        };
      }
      return null;
    };
  }

  usernameOccupied(control: AbstractControl): Observable<any> {
    this.loading = true;
    return this.userManagementService.checkIfUserExists(control.value).map(res => {
      this.loading = false;
      return res ? {usernameTaken: true} : null;
    });
  }
}
