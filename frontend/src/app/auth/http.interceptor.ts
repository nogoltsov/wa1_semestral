import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import {Router} from '@angular/router';

// FIXME: move to config file
const APP_BASE_URL = 'http://localhost:8000/api/v1';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // add authorization header with jwt token if available
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.token}`
        }
      });
    }

    // add baseUrl
    request = request.clone({url: `${APP_BASE_URL}/${request.url}`});

    return next.handle(request).do(event => {
    }, err => {
      if (err instanceof HttpErrorResponse && err.status === 401) {
        // handling auth errors
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
      }
    });
  }
}
