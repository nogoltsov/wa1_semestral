import {Injectable} from '@angular/core';
import {UserManagementService} from '../_services/user-management/user-management.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  constructor(private userManagementService: UserManagementService) {
  }

  get isAuthenticated(): boolean {
    const user = localStorage.getItem('currentUser');
    return !!user && !!JSON.parse(user).token;
  }

  register(name: string, username: string, password: string): Observable<string> {
    return this.userManagementService.registerUser({name, username, password})
      .map(response => {
        // register successful if there's a jwt token in the response
        if (!!response.user && !!response.user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(response.user));
        }
        return '';
      });
  }

  login(username: string, password: string): Observable<string> {
    return this.userManagementService.loginUser({username, password})
      .map(response => {
        // login successful if there's a jwt token in the response
        if (!!response.user && !!response.user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(response.user));
        }

        return '';
      });
  }

  // noinspection JSMethodCanBeStatic
  logout() {
    this.userManagementService.logoutUser().subscribe(response => {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
      },
      err => {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
      });
  }
}
