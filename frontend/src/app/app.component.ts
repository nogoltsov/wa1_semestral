import {Component} from '@angular/core';
import {AuthService} from './auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  get pageSize() {
    return localStorage.getItem('pageSize');
  }

  sizeItems = [5, 10, 20, 30];

  constructor(public authService: AuthService, private router: Router) {
    if (!localStorage.getItem('pageSize')) {
      localStorage.setItem('pageSize', '10');
    }
  }

  onLogout(): void {
    this.router.navigate(['login']);
  }

  onSelectPageSize(size: number): void {
    localStorage.setItem('pageSize', size + '');
    window.location.reload();
  }
}
