import * as moment from 'moment';
import * as R from 'ramda';

export interface IdentifiableEntity {
  id: string;
}

export interface Milestone extends IdentifiableEntity {
  name: string;
  description: string;
  creationDate: moment.Moment;
  startsAt: moment.Moment;
  endsAt: moment.Moment;
}

export interface Issue extends IdentifiableEntity {
  title: string;
  description: string;
  creationDate: moment.Moment;
  isResolved: boolean;
}

export interface User extends IdentifiableEntity {
  name: string;
  username: string;

  milestones: Milestone[];
}

export class PageableDatasource<T extends IdentifiableEntity> implements ItemsList<T> {

  items: T[];
  pagination: Pagination;

  readonly sortField: string;

  constructor(sortField: string, pageSize) {
    this.sortField = sortField;
    this.items = [];
    this.pagination = new RequestPagination(0, pageSize);
  }

  empty() {
    this.items = [];
    this.pagination.page = 0;
    this.pagination.pagesTotal = 0;
    this.pagination.itemsTotal = 0;
  }

  append(list: ItemsList<T>) {
    // merge old items with appended
    this.items = R.unionWith(R.eqBy(R.prop('id')), list.items, this.items);
    // sort items by creation date
    R.sortBy(R.prop(this.sortField))(this.items);
    // copy pagination
    this.pagination = Object.assign({}, list.pagination);
  }

}

export interface ItemsList<T extends IdentifiableEntity> {
  items: T[];
  pagination: Pagination;

  empty();
  append(list: ItemsList<T>);
}


export class RequestPagination implements Pagination {

  page: number;
  pageSize: number;
  pagesTotal: number;
  itemsTotal: number;

  constructor(page: number, pageSize: number) {
    this.page = page;
    this.pageSize = pageSize;
    this.pagesTotal = 0;
    this.itemsTotal = 0;

  }
}

export interface Pagination {
  page: number;
  pageSize: number;
  pagesTotal: number;
  itemsTotal: number;
}
