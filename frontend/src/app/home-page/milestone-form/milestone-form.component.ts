import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {MilestoneService} from '../../_services/milestone/milestone.service';
import {Observable} from 'rxjs/Observable';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-milestone-form',
  templateUrl: './milestone-form.component.html',
  styleUrls: ['./milestone-form.component.css']
})
export class MilestoneFormComponent implements OnInit {

  form: FormGroup;

  get name(): AbstractControl {
    return this.form.controls.name;
  }

  get startsAt(): AbstractControl {
    return this.form.controls.startsAt;
  }

  get endsAt(): AbstractControl {
    return this.form.controls.endsAt;
  }

  constructor(private milestoneService: MilestoneService,
              public activeModal: NgbActiveModal,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(5)]),
      description: new FormControl('', []),
      startsAt: new FormControl('', [Validators.required, this.dateValid]),
      endsAt: new FormControl('', [Validators.required, this.dateValid])
    });
  }

  onSubmit() {
    this.milestoneService.createMilestone({
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      startsAt: moment(this.form.controls.startsAt.value).format('YYYY-MM-DD'),
      endsAt: moment(this.form.controls.endsAt.value).format('YYYY-MM-DD')
    }).subscribe(
      res => {
        this.activeModal.close(true);
      },
      err => {
        this.toastr.error('Please reload page', 'Data fetching failed');
      }
    );
  }

  // noinspection JSMethodCanBeStatic
  dateValid(control: AbstractControl) {
    return moment(control.value).isValid() ? null : {dateValid: true};
  }
}
