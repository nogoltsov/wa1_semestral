import {Component, OnInit} from '@angular/core';
import {ItemsList, Milestone, PageableDatasource, RequestPagination} from '../app.types';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MilestoneFormComponent} from './milestone-form/milestone-form.component';
import {MilestoneService} from '../_services/milestone/milestone.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  milestonesData: ItemsList<Milestone> = new PageableDatasource<Milestone>('startsAt', this.pageSize);
  isLoading = false;

  get pagesTotal(): number {
    return this.milestonesData.pagination.pagesTotal;
  }

  get page(): number {
    return this.milestonesData.pagination.page;
  }

  get pageItems(): Milestone[] {
    return this.milestonesData.items.slice(this.page * this.pageSize, (this.page + 1) * this.pageSize);
  }

  get pageSize(): number {
    return Number(localStorage.getItem('pageSize'));
  }

  constructor(private milestoneService: MilestoneService,
              private modalService: NgbModal,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.retrieveMilestones(0);
  }

  onPreviousPageSelected(): void {
    this.retrieveMilestones(this.page - 1);
  }

  onNextPageSelected(): void {
    this.retrieveMilestones(this.page + 1);
  }

  onPageSelected(page: number): void {
    this.retrieveMilestones(page);
  }

  onMilestoneRemove(id: string): void {
    this.isLoading = true;
    this.milestoneService.removeMilestone(id)
      .subscribe(res => {
        const page = this.page;
        this.isLoading = false;
        this.milestonesData.empty();
        this.retrieveMilestones(page);
      })
    ;
  }

  onMilestoneOpen(milestone: Milestone): void {
    sessionStorage.setItem('milestone', JSON.stringify(milestone));
    this.router.navigate(['milestone', milestone.id]);
  }

  openNewMilestoneForm() {
    const modalRef = this.modalService.open(MilestoneFormComponent);
    modalRef.result.then(res => {
      if (res) {
        this.retrieveMilestones(this.page);
      }
    })
      .catch(err => {});
  }

  private retrieveMilestones(page: number) {
    this.isLoading = true;
    this.milestoneService.getMilestones(new RequestPagination(page, this.pageSize))
      .subscribe(res => {
        this.milestonesData.append(res);
        this.isLoading = false;
      }, err => {
        this.toastr.error('Please reload page', 'Data fetching failed');
      });
  }
}
