import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Issue} from '../../app.types';

@Component({
  selector: 'app-issue-card',
  templateUrl: './issue-card.component.html',
  styleUrls: ['./issue-card.component.css']
})
export class IssueCardComponent implements OnInit {

  @Input() issue: Issue;

  @Output() deleteIssue = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit() {
  }

}
