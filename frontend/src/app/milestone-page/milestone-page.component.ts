import {Component, OnInit} from '@angular/core';
import {Issue, ItemsList, Milestone, PageableDatasource, RequestPagination} from '../app.types';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MilestoneFormComponent} from '../home-page/milestone-form/milestone-form.component';
import {Router} from '@angular/router';
import {IssueService} from '../_services/issue/issues.service';
import {IssueFormComponent} from './issue-form/issue-form.component';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-milestone-page',
  templateUrl: './milestone-page.component.html',
  styleUrls: ['./milestone-page.component.css']
})
export class MilestonePageComponent implements OnInit {

  milestone: Milestone;

  issuesData: ItemsList<Issue> = new PageableDatasource<Issue>('createdDate', this.pageSize);
  isLoading = false;

  get itemsTotal(): number {
    return this.issuesData.pagination.itemsTotal;
  }

  get pagesTotal(): number {
    return this.issuesData.pagination.pagesTotal;
  }

  get page(): number {
    return this.issuesData.pagination.page;
  }

  get pageSize(): number {
    return Number(localStorage.getItem('pageSize'));
  }

  constructor(private issueService: IssueService,
              private modalService: NgbModal,
              private router: Router,
              private toastr: ToastrService) {

    if (!sessionStorage.getItem('milestone')) {
      this.router.navigate(['home']);
      return;
    }

    this.milestone = JSON.parse(sessionStorage.getItem('milestone')) as Milestone;
  }

  ngOnInit() {
    this.retrieveIssues(0);
  }

  onNextPageClicked(): void {
    this.retrieveIssues(this.page + 1);
  }

  onBackToHomeClicked(): void {
    this.router.navigate(['home']);
  }

  onIssueRemove(id: string): void {
    this.isLoading = true;
    this.issueService.removeIssue(this.milestone.id, id)
      .subscribe(res => {
        const page = this.page;
        this.isLoading = false;
        this.retrieveIssues(page);
      })
    ;
  }

  openNewIssueForm() {
    const modalRef = this.modalService.open(IssueFormComponent);
    modalRef.result.then(res => {
      if (res) {
        this.retrieveIssues(this.page);
      }
    })
      .catch(err => {
      });
  }

  private retrieveIssues(page: number) {
    this.isLoading = true;
    this.issueService.getIssues(this.milestone.id, new RequestPagination(page, this.pageSize))
      .subscribe(res => {
          this.issuesData.empty();
          this.issuesData.append(res);
          this.isLoading = false;
        },
        err => {
          this.toastr.error('Please reload page', 'Data fetching failed');
        })
    ;
  }

}
