import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup} from '@angular/forms';
import {IssueService} from '../../_services/issue/issues.service';
import {Milestone} from '../../app.types';

@Component({
  selector: 'app-issue-form',
  templateUrl: './issue-form.component.html',
  styleUrls: ['./issue-form.component.css']
})
export class IssueFormComponent implements OnInit {

  milestone: Milestone;
  form: FormGroup;

  constructor(private issueService: IssueService, public activeModal: NgbActiveModal) {
    if (!sessionStorage.getItem('milestone')) {
      this.activeModal.close(false);
      return;
    }
    this.milestone = JSON.parse(sessionStorage.getItem('milestone')) as Milestone;
  }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', []),
      description: new FormControl('', []),
    });
  }

  onSubmit() {
    this.issueService.createIssue(this.milestone.id, {
      title: this.form.controls.title.value,
      description: this.form.controls.description.value,
    }).subscribe(
      res => {
        this.activeModal.close(true);
      },
      err => {
        console.log('failed');
      }
    );
  }
}
