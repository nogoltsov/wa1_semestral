import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  moduleId: module.id,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;
  returnUrl: string;

  form: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(5)]),
      password: new FormControl('', [Validators.required, Validators.minLength(5)])
    });

    // reset login status
    this.authService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get isUsernameInvalid(): boolean {
    return !this.form.controls.name.valid;
  }

  get isPasswordInvalid(): boolean {
    return !this.form.controls.password.valid;
  }

  login() {
    this.loading = true;
    this.authService.login(this.form.controls.name.value, this.form.controls.name.value)
      .subscribe(res => {
          this.loading = false;
          if (!!this.route.snapshot.params['returnUrl']) {
            // navigate to previous page (from returnUrl param)
            this.router.navigate([this.route.snapshot.params['returnUrl']]);
          } else {
            // navigate to the Home page
            this.router.navigate(['/home']);
          }
        },
        error => {
          this.toastr.error('Please try again', 'Authentication failed');
          this.loading = false;
        }
      );
  }
}
