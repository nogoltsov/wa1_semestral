<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180122035945 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE board CHANGE description description VARCHAR(1000) DEFAULT NULL, CHANGE starts_at starts_at DATE DEFAULT NULL, CHANGE ends_at ends_at DATE DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE board CHANGE description description VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, CHANGE starts_at starts_at DATE NOT NULL, CHANGE ends_at ends_at DATE NOT NULL');
    }
}
