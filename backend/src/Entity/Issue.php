<?php

namespace App\Entity;

use App\Controller\Api\v1\DTO\issue\IssueCreationRequest;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Issue.php
 * Represents a database issue entity model.
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\IssueRepository")
 */
class Issue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var string entity identificator
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Milestone")
     * @ORM\JoinColumn(name="milestone_id", referencedColumnName="id")
     *
     * @var Milestone issue related milestone
     */
    private $milestone;

    /**
     * @ORM\Column(type="string", length=64)
     *
     * @var string issue title text
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     *
     * @var string issue description text
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     *
     * @var boolean describes if issue was resolved
     */
    private $isResolved;

    /**
     * @ORM\Column(type="date")
     *
     * @var DateTime date when issue was created
     */
    private $creationDate;


    /**
     * @param IssueCreationRequest $creationRequest
     * @param Milestone $milestone
     * @return Issue
     */
    public static function of(IssueCreationRequest $creationRequest, Milestone $milestone)
    {
        $issue = new Issue();
        $issue->setTitle($creationRequest->getTitle());
        $issue->setDescription($creationRequest->getDescription());
        $issue->setIsResolved(false);
        $issue->setCreationDate(new DateTime());
        $issue->setMilestone($milestone);
        return $issue;
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getMilestone()
    {
        return $this->milestone;
    }

    public function setMilestone($milestone): void
    {
        $this->milestone = $milestone;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getisResolved()
    {
        return $this->isResolved;
    }

    public function setIsResolved($isResolved): void
    {
        $this->isResolved = $isResolved;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate): void
    {
        $this->creationDate = $creationDate;
    }

}
