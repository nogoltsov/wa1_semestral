<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User.php
 * Represents a database user entity model.
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var string entity identificator
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     *
     * @var string user's username
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     *
     * @var string user's encoded password
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     *
     * @var boolean identifies if user's account is active and valid
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * constructor "overloading"
     */
    public static function init($username, $password)
    {
        $instance = new self();
        $instance->username = $username;
        $instance->password = $password;
        return $instance;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        // null because of BCrypt
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized);
    }
}
