<?php

namespace App\Entity;

use App\Controller\Api\v1\DTO\milestone\MilestoneCreationRequest;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Milestone.php
 * Represents a database milestone entity model.
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\MilestoneRepository")
 * @ORM\Table(name="milestone",
 *    uniqueConstraints={
 *        @ORM\UniqueConstraint(name="user__milestone_name__unique",
 *            columns={"id", "user_id"})
 *    }
 * )
 */
class Milestone
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var string entity identificator
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @var User milestone owner
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=64)
     *
     * @var string milestone name text
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     *
     * @var string milestone description text
     */
    private $description;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     * @var DateTime milestone start date&time
     */
    private $startsAt;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     * @var DateTime milestone end date&time
     */
    private $endsAt;

    /**
     * @ORM\Column(type="date")
     *
     * @var DateTime milestone creation date&time
     */
    private $creationDate;

    /**
     * @ORM\OneToMany(targetEntity="Issue", mappedBy="milestone", cascade={"remove"})
     *
     * @var array issues list
     */
    private $issues;

    /**
     * @param MilestoneCreationRequest $creationRequest
     * @param User $user
     * @return Milestone
     */
    public static function of(MilestoneCreationRequest $creationRequest, User $user)
    {
        $milestone = new Milestone();
        $milestone->setName($creationRequest->getName());
        $milestone->setStartsAt(date_create_from_format('Y-m-d', $creationRequest->getStartsAt()));
        $milestone->setEndsAt(date_create_from_format('Y-m-d', $creationRequest->getEndsAt()));
        $milestone->setCreationDate(new DateTime());
        $milestone->setUser($user);
        return $milestone;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): void
    {
        $this->user = $user;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getStartsAt()
    {
        return $this->startsAt;
    }

    public function setStartsAt($startsAt): void
    {
        $this->startsAt = $startsAt;
    }

    public function getEndsAt()
    {
        return $this->endsAt;
    }

    public function setEndsAt($endsAt): void
    {
        $this->endsAt = $endsAt;
    }

    public function getIssues(): array
    {
        return $this->issues;
    }

    public function setIssues(array $issues): void
    {
        $this->issues = $issues;
    }
}
