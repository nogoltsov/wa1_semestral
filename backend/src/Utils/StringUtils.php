<?php

namespace App\Utils;


class StringUtils
{
    /**
     * Checks if string is null or empty
     */
    public static function isEmptyString($data)
    {
        return (trim($data) === "" or $data === null);
    }
}