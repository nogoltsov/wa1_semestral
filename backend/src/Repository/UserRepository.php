<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * UserRepository.php
 * Repository layer component representing User ORM (@see User)
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Creates an instance of object
     *
     * @param User $user object to save
     * @return User persisted object
     */
    public function create(User $user): User
    {
        $this->_em->persist($user);
        $this->_em->flush($user);

        return $user;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @return UserInterface|null
     */
    public function loadUserByUsername($username)
    {
        return $this->findOneBy(['username' => $username]);
    }
}
