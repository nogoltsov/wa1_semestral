<?php

namespace App\Repository;


class ListResult
{
    private $results;

    private $page;

    private $pageSize;

    private $itemsTotal;

    /**
     * ListResult constructor.
     * @param $results
     * @param $page
     * @param $pageSize
     * @param $itemsTotal
     */
    public function __construct($results, $page, $pageSize, $itemsTotal)
    {
        $this->results = $results;
        $this->page = $page;
        $this->pageSize = $pageSize;
        $this->itemsTotal = $itemsTotal;
    }

    /**
     * @return mixed
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return int
     */
    public function getItemsTotal(): int
    {
        return $this->itemsTotal;
    }

}