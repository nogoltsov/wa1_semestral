<?php

namespace App\Repository;

use App\Controller\Api\v1\DTO\RequestPagination;
use App\Entity\Milestone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * MilestoneRepository.php
 * Repository layer component representing Milestone ORM (@see Milestone)
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 */
class MilestoneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Milestone::class);
    }

    /**
     * Creates an instance of object
     *
     * @param Milestone $milestone object to save
     * @return Milestone persisted object
     */
    public function create(Milestone $milestone): Milestone
    {
        $this->_em->persist($milestone);
        $this->_em->flush();
        return $milestone;
    }

    /**
     * Retrieve list of user milestones with paging
     *
     * @param string $username owner's username
     * @param RequestPagination $pagination request pagination
     * @return ListResult objects result set
     */
    public function listUserMilestones(string $username, RequestPagination $pagination): ListResult
    {
        // build query
        $query = $this->createQueryBuilder('m')
            ->innerJoin('m.user', 'u')
            ->where('u.username = :username')->setParameter('username', $username)
            ->orderBy('m.creationDate', 'DESC')
            ->getQuery();
        // get total counts
        $paginator = new Paginator($query);
        $totalItems = count($paginator);
        // set needed range values
        $paginator
            ->getQuery()
            ->setMaxResults($pagination->getPageSize())
            ->setFirstResult($pagination->getFirstResult());
        // get items array
        $items = iterator_to_array($paginator->getIterator());
        // return paginator
        return new ListResult($items, $pagination->getPage(), $pagination->getPageSize(), $totalItems);
    }

    /**
     * Retrieve user's milestone by its identificator
     *
     * @param string $username owner's username
     * @param int $milestoneId milestone identificator
     * @return Milestone objects result
     *
     * @throws NonUniqueResultException If the query result is not unique.
     * @throws NoResultException        If the query returned no result and hydration mode is not HYDRATE_SINGLE_SCALAR.
     */
    public function getUserMilestone(string $username, int $milestoneId): Milestone
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->createQueryBuilder('m')
            ->innerJoin('m.user', 'u')
            ->where('m.id = :id')->setParameter('id', $milestoneId)
            ->andWhere('u.username = :username')->setParameter('username', $username)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Retrieve user's milestone by its name
     *
     * @param string $username owner's username
     * @param string $milestoneName milestone name
     * @return array milestone objects result set
     *
     */
    public function getUserMilestonesByName(string $username, string $milestoneName): array
    {
        return $this->createQueryBuilder('m')
            ->innerJoin('m.user', 'u')
            ->where('m.name = :name')->setParameter('name', $milestoneName)
            ->andWhere('u.username = :username')->setParameter('username', $username)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Delete an instance of object
     *
     * @param Milestone $milestone object to remove
     * @return int removed instances number
     */
    public function remove(Milestone $milestone): int
    {
        $this->_em->remove($milestone);
        $this->_em->flush();
        // return number of updated entities
        return 1;
    }
}
