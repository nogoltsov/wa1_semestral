<?php

namespace App\Repository;

use App\Controller\Api\v1\DTO\RequestPagination;
use App\Entity\Issue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * IssueRepository.php
 * Repository layer component representing Issue ORM (@see Issue)
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 */
class IssueRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Issue::class);
    }

    /**
     * Creates an instance of object
     *
     * @param Issue $issue object to save
     * @return Issue persisted object
     */
    public function create(Issue $issue): Issue
    {
        $this->_em->persist($issue);
        $this->_em->flush();
        return $issue;
    }

    /**
     * Delete an instance of object
     *
     * @param Issue $issue object to remove
     * @return int removed instances number
     */
    public function remove(Issue $issue): int
    {
        $this->_em->remove($issue);
        $this->_em->flush();
        // return number of updated entities
        return 1;
    }

    /**
     * Retrieve list of milestone issues with paging
     *
     * @param string $milestoneId related milestone identificator (search param)
     * @param RequestPagination $pagination request pagination
     * @return ListResult objects result set
     */
    public function listMilestoneIssues(string $milestoneId, RequestPagination $pagination): ListResult
    {
        // build query
        $query = $this->createQueryBuilder('i')
            ->innerJoin('i.milestone', 'm')
            ->where('m.id = :id')->setParameter('id', $milestoneId)
            ->orderBy('i.creationDate', 'DESC')
            ->getQuery();
        // get total counts
        $paginator = new Paginator($query);
        $totalItems = count($paginator);
        // set needed range values
        $paginator
            ->getQuery()
            ->setMaxResults($pagination->getPageSize())
            ->setFirstResult($pagination->getFirstResult());
        // get items array
        $items = iterator_to_array($paginator->getIterator());
        // return paginator
        return new ListResult($items, $pagination->getPage(), $pagination->getPageSize(), $totalItems);
    }
}
