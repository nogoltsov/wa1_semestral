<?php

namespace App\Controller\Api\v1;

use App\Controller\Api\v1\DTO\auth\RegisterRequest;
use App\Entity\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * AuthenticationController.php
 * Controller layer component providing  REST API for users authentication
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 */
class AuthenticationController extends BaseController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        // set password encoder
        $this->encoder = $encoder;
    }

    /**
     * Resolves if user already exists or not
     *
     * @Route("/api/v1/auth/check-user-existence")
     * @Method("GET")
     */
    public function checkUserExistence(Request $request)
    {
        // get username
        $username = $request->query->get('username');
        $user = $this->getUserRepository()->findOneBy(['username' => $username]);
        return $this->createApiResponse($user instanceof User, 200);
    }

    /**
     * Register user and returns his authentication token
     *
     * @Route("/api/v1/auth/register", defaults={"_format": "json"})
     * @Method("POST")
     */
    public function signup(Request $request)
    {
        // get repository
        $repository = $this->getUserRepository();
        // deserialize request body
        $requestBody = $this->deserialize($request->getContent(), RegisterRequest::class);
        // validate request body
        if (!$requestBody->isValid()) {
            // return bad request response
            throw new BadRequestHttpException("Wrong request format");
        }
        // create user instance
        $username = $requestBody->getUsername();
        $password = $this->get('security.password_encoder')->encodePassword(new User(), $requestBody->getPassword());
        $user = User::init($username, $password);
        // persist data
        $repository->create($user);
        // generate user`s token
        $token = $this->generateJWT($username, 3600);
        // return user data
        return $this->createApiResponse(["user" => ["token" => $token]], 201);
    }

    /**
     * Log user in and returns his authentication token
     *
     * @Route("/api/v1/auth/login", defaults={"_format": "json"})
     * @Method("POST")
     */
    public function login(Request $request)
    {
        // deserialize request body
        $requestBody = $this->deserialize($request->getContent(), RegisterRequest::class);
        // validate request body
        if (!$requestBody->isValid()) {
            // return bad request response
            throw new BadRequestHttpException("Wrong request format");
        }
        // find user
        $user = $this->getUserRepository()->loadUserByUsername($requestBody->getUsername());
        // check if exists
        if (!$user) {
            // return authentication failed response
            throw new AccessDeniedHttpException("Authentication failed");
        }
        // check credentials
        if (!$this->get('security.password_encoder')->isPasswordValid($user, $requestBody->getPassword())) {
            // return authentication failed response
            throw new AccessDeniedHttpException("Authentication failed");
        }
        // generate JWT
        $token = $this->generateJWT($user->getUsername(), 3600);
        // return user data
        return $this->createApiResponse(["user" => ["token" => $token]], 200);
    }

    /**
     * Log user out and invalidate his authentication token
     *
     * @Route("/api/v1/auth/logout", defaults={"_format": "json"})
     * @Method("POST")
     */
    public function logout(Request $request)
    {
        if ($this->isUserLoggedIn()) {
            $this->logoutUser();
        }
        return $this->createApiResponse("Successfully logged out", 200);
    }
}