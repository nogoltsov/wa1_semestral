<?php

namespace App\Controller\Api\v1;

use App\Controller\Api\v1\DTO\issue\IssueCreationRequest;
use App\Controller\Api\v1\DTO\issue\IssueDTO;
use App\Controller\Api\v1\DTO\issue\IssueListResponse;
use App\Entity\Issue;
use App\Entity\Milestone;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * IssueController.php
 * Controller layer component providing  REST API for @see Issue object
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 */
class IssueController extends BaseController
{

    /**
     * Creates milestone issue
     *
     * @Route("/api/v1/milestone/{milestoneId}/issue")
     * @Method("POST")
     */
    public function createIssue(Request $request, string $milestoneId)
    {
        // validate that issue is related with milestone and user
        $this->validateIssueRelation($milestoneId);
        // deserialize request body
        $requestBody = $this->deserialize($request->getContent(), IssueCreationRequest::class);
        // validate request body
        if (!$requestBody->isValid()) {
            // create bad request response
            throw new BadRequestHttpException("Wrong request format");
        }
        // create domain entity
        /** @noinspection PhpParamsInspection */
        $issue = Issue::of($requestBody, $this->getMilestoneRepository()->find($milestoneId));
        // save domain entity
        $issue = $this->getIssueRepository()->create($issue);
        // map to response entity
        $response = new IssueDTO($issue);
        // return response
        return $this->createApiResponse($response, 200);
    }

    /**
     * Retrieves all milestone issues with pagination in request and response
     *
     * @Route("/api/v1/milestone/{milestoneId}/issue")
     * @Method("GET")
     */
    public function listIssues(string $milestoneId)
    {
        // validate that issue is related with milestone and user
        $this->validateIssueRelation($milestoneId);
        // get request pagination
        $pagination = $this->getRequestPagination();
        // retrieve list result
        $listResult = $this->getIssueRepository()->listMilestoneIssues($milestoneId, $pagination);
        // map to response entity
        $response = new IssueListResponse($listResult);
        // return response
        return $this->createApiResponse($response, 200);
    }

    /**
     * Retrieves milestone issue by its identificator
     *
     * @Route("/api/v1/milestone/{milestoneId}/issue/{issueId}")
     * @Method("GET")
     */
    public function getIssue(string $milestoneId, string $issueId)
    {
        // validate that issue is related with milestone and user
        $this->validateIssueRelation($milestoneId);

        $issue = $this->getIssueRepository()->find($issueId);
        if (!($issue instanceof Issue) || $issue->getMilestone()->getId() != $milestoneId) {
            // create not found response
            throw new NotFoundHttpException("Issue not found");
        }
        // map to response entity
        $response = new IssueDTO($issue);
        // return response
        return $this->createApiResponse($response, 200);
    }

    /**
     * Removes milestone issue using its identificator
     *
     * @Route("/api/v1/milestone/{milestoneId}/issue/{issueId}")
     * @Method("DELETE")
     */
    public function removeIssue(string $milestoneId, string $issueId)
    {
        // validate that issue is related with milestone and user
        $this->validateIssueRelation($milestoneId);
        // find entity
        $issue = $this->getIssueRepository()->find($issueId);
        if (!($issue instanceof Issue) || $issue->getMilestone()->getId() != $milestoneId) {
            // create not found response
            throw new NotFoundHttpException("Issue not found");
        }
        // remove entity
        $this->getIssueRepository()->remove($issue);
        // return OK response
        return $this->createApiResponse('OK', 200);
    }

    private function validateIssueRelation(string $milestoneId): void
    {
        $milestone = $this->getMilestoneRepository()->find($milestoneId);
        if (!($milestone instanceof Milestone)) {
            throw new NotFoundHttpException("Milestone not found");
        }

        $user = $this->getUser();
        if ($milestone->getUser()->getId() != $user->getId()) {
            // create access denied
            throw new AccessDeniedHttpException("Access denied");
        }
    }
}