<?php

namespace App\Controller\Api\v1\DTO\issue;


use App\Entity\Issue;
use App\Entity\Milestone;

class IssueDTO
{
    private $id;

    private $title;

    private $description;

    private $isResolved;

    private $creationDate;

    /**
     * MilestoneDTO constructor.
     * @param milestone - domain entity
     */
    public function __construct(Issue $issue)
    {
        $this->id = $issue->getId();
        $this->title = $issue->getTitle();
        $this->description = $issue->getDescription();
        $this->isResolved = $issue->getisResolved();
        $this->creationDate = $issue->getCreationDate();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getisResolved()
    {
        return $this->isResolved;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
}