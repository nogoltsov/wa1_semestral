<?php

namespace App\Controller\Api\v1\DTO\issue;


class IssueCreationRequest
{
    private $title;

    private $description;

    /**
     * IssueCreationRequest constructor.
     * @param $title
     * @param $description
     */
    public function __construct($title, $description)
    {
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * validates request entity
     */
    public function isValid(): bool
    {
        return true;
    }
}