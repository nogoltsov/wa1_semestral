<?php

namespace App\Controller\Api\v1\DTO\milestone;


class MilestoneCreationRequest
{
    private $name;

    private $description;

    private $startsAt;

    private $endsAt;

    /**
     * MilestoneCreationRequest constructor.
     * @param $name
     * @param $description
     * @param $startsAt
     * @param $endsAt
     */
    public function __construct($name, $description, $startsAt, $endsAt)
    {
        $this->name = $name;
        $this->description = $description;
        $this->startsAt = $startsAt;
        $this->endsAt = $endsAt;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getStartsAt(): string
    {
        return $this->startsAt;
    }

    /**
     * @return string
     */
    public function getEndsAt(): string
    {
        return $this->endsAt;
    }

    /**
     * validates request entity
     */
    public function isValid(): bool
    {
        return true;
    }
}