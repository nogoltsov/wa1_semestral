<?php

namespace App\Controller\Api\v1\DTO\milestone;


use App\Entity\Milestone;

class MilestoneDTO
{
    private $id;

    private $name;

    private $description;

    private $startsAt;

    private $endsAt;

    private $creationDate;

    /**
     * MilestoneDTO constructor.
     * @param milestone - domain entity
     */
    public function __construct(Milestone $milestone)
    {
        $this->id = $milestone->getId();
        $this->name = $milestone->getName();
        $this->description = $milestone->getDescription();
        $this->startsAt = $milestone->getStartsAt();
        $this->endsAt = $milestone->getEndsAt();
        $this->creationDate = $milestone->getCreationDate();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return \DateTime
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @return \DateTime
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }



}