<?php

namespace App\Controller\Api\v1\DTO\milestone;


use App\Controller\Api\v1\DTO\ListResponse;

class MilestoneListResponse extends ListResponse
{
    function mapItem($item)
    {
        return new MilestoneDTO($item);
    }
}