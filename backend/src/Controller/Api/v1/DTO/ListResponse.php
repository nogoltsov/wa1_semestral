<?php

namespace App\Controller\Api\v1\DTO;

use App\Repository\ListResult;

abstract class ListResponse
{
    private $items;

    private $pagination;

    /**
     * BoardListResponse constructor.
     * @param $result - repository list result
     */
    public function __construct(ListResult $result)
    {
        // map repository objects to DTOs
        $this->items = array_map([$this, 'mapItem'], $result->getResults());
        // initialize pagination
        $this->pagination = new ResponsePagination($result->getPage(), $result->getPageSize(), $result->getItemsTotal());
    }


    abstract function mapItem($item);

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return ResponsePagination
     */
    public function getPagination(): ResponsePagination
    {
        return $this->pagination;
    }
}