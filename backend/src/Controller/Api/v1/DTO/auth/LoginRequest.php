<?php

namespace App\Controller\Api\v1\DTO\auth;


use App\Utils\StringUtils;

class LoginRequest
{
    private $username;

    private $password;

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * validates request entity
     */
    public function isValid(): bool
    {
        return true;
    }
}