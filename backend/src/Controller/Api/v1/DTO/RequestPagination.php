<?php

namespace App\Controller\Api\v1\DTO;


class RequestPagination
{

    private $page;

    private $pageSize;

    /**
     * RequestPagination constructor.
     * @param $page
     * @param $pageSize
     */
    public function __construct(int $page, int $pageSize)
    {
        // check for negative values
        if ($page < 0) {
            $page = 0;
        }
        if ($pageSize < 1) {
            $pageSize = 1;
        }
        // initialize class fields
        $this->page = $page;
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * min index of entity in list response
     * @return int
     */
    public function getFirstResult(): int
    {
        return $this->page * $this->pageSize;
    }

}