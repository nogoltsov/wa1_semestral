<?php

namespace App\Controller\Api\v1\DTO;


class ResponsePagination
{

    private $page;

    private $pageSize;

    private $pagesTotal;

    private $itemsTotal;

    /**
     * ResponsePagination constructor.
     * @param $page
     * @param $pageSize
     * @param $itemsTotal
     */
    public function __construct($page, $pageSize, $itemsTotal)
    {
        $this->page = $page;
        $this->pageSize = $pageSize;
        $this->itemsTotal = $itemsTotal;
        $this->pagesTotal = ceil($itemsTotal / $pageSize);;
    }


    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return int
     */
    public function getPagesTotal(): int
    {
        return $this->pagesTotal;
    }

    /**
     * @return int
     */
    public function getItemsTotal(): int
    {
        return $this->itemsTotal;
    }

}