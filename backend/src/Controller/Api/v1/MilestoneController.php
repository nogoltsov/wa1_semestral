<?php

namespace App\Controller\Api\v1;

use App\Controller\Api\v1\DTO\milestone\MilestoneCreationRequest;
use App\Controller\Api\v1\DTO\milestone\MilestoneDTO;
use App\Controller\Api\v1\DTO\milestone\MilestoneListResponse;
use App\Entity\Milestone;
use Exception;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * MilestoneController.php
 * Controller layer component providing  REST API for @see Milestone object
 * @author Nikolai Ogoltsov <nogoltsov@gmail.com>
 */
class MilestoneController extends BaseController
{
    /**
     * Creates milestone instance with specified parameters. Returns created milestone instance
     *
     * @Route("/api/v1/milestone")
     * @Method("POST")
     */
    public function createMilestone(Request $request)
    {
        // deserialize request body
        $requestBody = $this->deserialize($request->getContent(), MilestoneCreationRequest::class);
        // validate request body
        if (!$requestBody->isValid()) {
            // create bad request response
            throw new BadRequestHttpException("Wrong request format");
        }
        // get current user
        $user = $this->getUser();
        // get repository
        $repository = $this->getMilestoneRepository();
        // get user's boards with same name
        $boards_with_same_name = $repository->getUserMilestonesByName($user->getUsername(), $requestBody->getName());
        if (count($boards_with_same_name) > 0) {
            // create bad request response
            throw new BadRequestHttpException("User has already had board with this name");
        }
        // create domain entity
        $board = Milestone::of($requestBody, $user);
        // save domain entity
        $board = $repository->create($board);
        // map to response entity
        $response = new MilestoneDTO($board);
        // return response
        return $this->createApiResponse($response, 200);
    }

    /**
     * Retrieves all user milestones with pagination in request and response
     *
     * @Route("/api/v1/milestone")
     * @Method("GET")
     */
    public function listMilestones()
    {
        // get current user
        $user = $this->getUser();
        // get repository
        $repository = $this->getMilestoneRepository();
        // get request pagination
        $pagination = $this->getRequestPagination();
        // retrieve list result
        $listResult = $repository->listUserMilestones($user->getUsername(), $pagination);
        // map to response entity
        $response = new MilestoneListResponse($listResult);
        // return response
        return $this->createApiResponse($response, 200);
    }


    /**
     * Retrieves milestone by its identificator
     *
     * @Route("/api/v1/milestone/{id}")
     * @Method("GET")
     */
    public function getMilestone($id)
    {
        // get current user
        $user = $this->getUser();
        // get repository
        $repository = $this->getMilestoneRepository();
        try {
            // retrieve entity
            $board = $repository->getUserMilestone($user->getUsername(), $id);
        } catch (Exception $e) {
            // create not found response
            throw new NotFoundHttpException($e->getMessage());
        }
        // map to response entity
        $response = new MilestoneDTO($board);
        // return response
        return $this->createApiResponse($response, 200);
    }

    /**
     * Removes milestone by its identificator
     *
     * @Route("/api/v1/milestone/{id}")
     * @Method("DELETE")
     */
    public function removeMilestone($id)
    {
        // retrieve and check milestone
        $milestone = $this->getMilestoneRepository()->find($id);
        if (!($milestone instanceof Milestone)) {
            // entity not found
            throw new NotFoundHttpException("Milestone not found");
        }
        // retrieve and check user relation
        $user = $this->getUser();
        if ($milestone->getUser()->getId() != $user->getId()) {
            // create access denied
            throw new AccessDeniedHttpException("Access denied");
        }
        // remove entity
        $this->getMilestoneRepository()->remove($milestone);
        // return OK response
        return $this->createApiResponse('OK', 200);
    }

}