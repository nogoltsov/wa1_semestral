<?php

namespace App\Controller\Api\v1;

use App\Controller\Api\v1\DTO\RequestPagination;
use App\Entity\User;
use App\Repository\IssueRepository;
use App\Repository\MilestoneRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

abstract class BaseController extends Controller
{
    /**
     * Is the current user logged in?
     *
     * @return boolean
     */
    public function isUserLoggedIn()
    {
        return $this->container->get('security.authorization_checker')
            ->isGranted('IS_AUTHENTICATED');
    }

    /**
     * Logs this user into the system
     *
     * @param User $user
     */
    public function loginUser(User $user): string
    {
        return $this->container
            ->get('lexik_jwt_authentication.handler.authentication_success')
            ->handleAuthenticationSuccess($user);
    }

    /**
     * Logs user out
     * @Security("is_authenticated()")
     */
    public function logoutUser()
    {
        $this->get('security.token_storage')->setToken(null);
        return $this->container->get('request_stack')->getCurrentRequest()->getSession()->invalidate();
    }

    /**
     * @return UserRepository
     */
    protected function getUserRepository(): UserRepository
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->getDoctrine()
            ->getRepository('App:User');
    }

    /**
     * @return MilestoneRepository
     */
    protected function getMilestoneRepository(): MilestoneRepository
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->getDoctrine()
            ->getRepository('App:Milestone');
    }

    /**
     * @return IssueRepository
     */
    protected function getIssueRepository(): IssueRepository
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->getDoctrine()
            ->getRepository('App:Issue');
    }

    /**
     * @return RequestPagination
     */
    protected function getRequestPagination(): RequestPagination
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $page = $request->query->get('page', 0);
        $pageSize = $request->query->get('pageSize', 10);

        return new RequestPagination($page, $pageSize);
    }

    protected function generateJWT($username, $duration)
    {
        return $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $username,
                'exp' => time() + $duration
            ]);
    }

    protected function createApiResponse($data, $statusCode = 200)
    {
        $json = $this->serialize($data);

        return new Response($json, $statusCode, array(
            'Content-Type' => 'application/json'
        ));
    }

    protected function serialize($data, $format = 'json')
    {
        return $this->container->get('serializer')
            ->serialize($data, $format);
    }

    protected function deserialize($data, $type, $format = "json")
    {
        try {
            return $this->container->get('serializer')->deserialize($data, $type, $format);
        } catch (\Exception $e) {
            throw new BadRequestHttpException("Can`t parse response data", $e);
        }
    }


}
