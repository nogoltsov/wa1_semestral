<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180122140325 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE issue ADD milestone_id INT DEFAULT NULL, ADD title VARCHAR(64) NOT NULL, ADD description VARCHAR(1000) DEFAULT NULL, ADD is_resolved TINYINT(1) DEFAULT \'0\' NOT NULL, ADD creation_date DATE NOT NULL');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E4B3E2EDA FOREIGN KEY (milestone_id) REFERENCES milestone (id)');
        $this->addSql('CREATE INDEX IDX_12AD233E4B3E2EDA ON issue (milestone_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233E4B3E2EDA');
        $this->addSql('DROP INDEX IDX_12AD233E4B3E2EDA ON issue');
        $this->addSql('ALTER TABLE issue DROP milestone_id, DROP title, DROP description, DROP is_resolved, DROP creation_date');
    }
}

