<?php

namespace App\Controller\Api\v1\DTO\issue;

use App\Controller\Api\v1\DTO\ListResponse;

class IssueListResponse extends ListResponse
{
    function mapItem($item)
    {
        return new IssueDTO($item);
    }
}
